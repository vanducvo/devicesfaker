function publisher(client, topic, data){
    let payload = Buffer.from(JSON.stringify(data));
    client.publish(topic, payload, {qos: 2});
}

module.exports = publisher;