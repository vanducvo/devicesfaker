# Devices Faker cho Đồ án đa ngành

## Thiết đặt trong file .env
1. **MQTT_SERVER**: link tới server EMQX `tcp://0.0.0.0:1883`
2. **TIME_CHANGE**: thời gian thay đổi thông số cảm biết `ms`
3. **TIME_SEND**: thời gian gửi định kỳ
4. **MOIS_TOPIC**: topic thiết bị gửi thông tin của cảm biết độ ẩm đất
5. **SPEAKER_TOPIC**: topic thiết bị nhận thông tin

## Sau khi thiết đặt
1. install dependencies: `yarn install`
2. run test : `npm run test` hoặc`yarn run test`
## Run
1. với `nodemon`: `npm run dev` hoặc `yarn run dev`
2. với `node`: `npm start` hoặc `yarn start`

## Tính năng fake thông số  SoilMoisture
## Tính năng điều kiển motor
## Dashboard ở  `<yourip>:8888`
