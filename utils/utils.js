const ramdom = require('random');

function generateValueSoilMoisture(){
	return [ramdom.int(0, 1023)];
}

function generateValueTempHumi(){
	return [ramdom.int(0, 100), ramdom.int(0, 100)];
}

function conditionLightD(payload, topic){
	if (
		!payload.values[0].match(/^[0-1]$/) || 
		!payload.values[1].match(/^\d+$/) ||
		!(Number(payload.values[1]) <= 255) ||
		topic !== process.env.LIGHTD_TOPIC
	){
		return 0;
	}

	let id = payload.device_id;

	if(id === "LightD"){
		return id;
	}

	return 0;
}

function extractorLightD(payload){
	return [Number(payload.values[1]), Number(payload.values[0])]
}

function conditionSpeaker(payload, topic){
	if (
		!payload.values[0].match(/^[0-1]$/) || 
		!payload.values[1].match(/^\d+$/) ||
		!(Number(payload.values[1]) <= 5000) ||
		topic !== process.env.SPEAKER_TOPIC
	){
		return 0;
	}

	let id = payload.device_id;

	if(id === "Speaker"){
		return id;
	}

	return 0;
}

function extractorSpeaker(payload){
	return [Number(payload.values[1]), Number(payload.values[0])]
}

exports.generateValueSoilMoisture = generateValueSoilMoisture;
exports.conditionSpeaker = conditionSpeaker;
exports.extractorSpeaker = extractorSpeaker;
exports.conditionLightD = conditionLightD;
exports.extractorLightD = extractorLightD;
exports.generateValueTempHumi = generateValueTempHumi;