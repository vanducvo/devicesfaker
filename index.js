require('dotenv').config();

const express = require('express');
const app = express()
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const path = require('path');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');

// Template
app.set('view engine', 'ejs');
app.use('/public', serveStatic(path.resolve(__dirname, 'public')));

const {connect, getBroker} = require('./controllers/mqtt');
const faker = require('./services/faker');
const publisher = require('./services/publisher');
const subcriber = require('./services/subcriber');

// Models
const SoilMoisture = require('./devices/SoilMoisture');
const Speaker = require('./devices/Speaker');
const TempHumi = require('./devices/TempHumi');
const LightD = require('./devices/LightD');

// Utils
const utils = require('./utils/utils');

// Enviroment
let url = process.env.MQTT_SERVER;
let topics = [
    process.env.MOIS_TOPIC,
    process.env.SPEAKER_TOPIC,
    process.env.TEMP_HUMI_TOPIC,
    process.env.LIGHTD_TOPIC
];
let clientID = process.env.CLIENT_ID;
let clientName = process.env.CLIENT_NAME;
let clientPassword = process.env.CLIENT_PASSWORD
let timeChange = Number(process.env.TIME_CHANGE);

// Server with 2th args is list topic output device
connect(url, [topics[1], topics[3]], clientID, clientName, clientPassword);

// SoilMoisture
let moisMapper = new Map();
let mois = new SoilMoisture('Mois');
moisMapper.set('Mois', mois);

mois.on('change', data => {
    io.emit('moischange', data)
    publisher(getBroker(), topics[0], [data]);
});

faker(moisMapper.values(), utils.generateValueSoilMoisture, timeChange);

// Temp Humi
let tempHumiMapper = new Map();
let tempHumi = new TempHumi('TempHumi');
tempHumiMapper.set('TempHumi', tempHumi);
tempHumi.on('change', data => {
    io.emit('temphumichange', data)
    publisher(getBroker(), topics[2] , [data]);
});

faker(tempHumiMapper.values(), utils.generateValueTempHumi, timeChange);

// Motor
let speakerMapper = new Map();
let speaker = new Speaker('Speaker');
speakerMapper.set('Speaker', speaker)
speaker.on('change', data => {
    io.emit('speakerchange', data);
});

subcriber(getBroker(), speakerMapper, utils.conditionSpeaker, utils.extractorSpeaker);

// Send Interval For Speaker
setInterval(() => {
    publisher(getBroker(), topics[1], [speaker.toJSON()]);
}, process.env.TIME_CHANGE);

// Light
let lightdMapper = new Map();
let lightd = new LightD('LightD');
lightdMapper.set('LightD', lightd)
lightd.on('change', data => {
    io.emit('lightchange', data);
});

subcriber(getBroker(), lightdMapper, utils.conditionLightD, utils.extractorLightD);

// Send Interval For LightD
setInterval(() => {
    publisher(getBroker(), topics[3], [lightd.toJSON()]);
}, process.env.TIME_CHANGE);

app.get('/', function(req, res){
    res.render('index.ejs', {
        mois_devices: [...moisMapper.values()],
        speaker_devices: [...speakerMapper.values()]
    });
});

app.post('/control', bodyParser.json(), function(req, res){
    let data = req.body;
    let device = speakerMapper.get(data.id);
    if(device){
        device.setValue(data.level, data.status);
        publisher(getBroker(), topics[1], [device.toJSON()]);
    }
    res.status(200).end();
});

io.on('connection', (socket) => {
    
});

server.listen(8888, () => {
    console.log('http://localhost:8888')
});