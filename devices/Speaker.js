const {EventEmitter} = require('events');
/**
 * Speaker device
 */
class Speaker extends EventEmitter{
    constructor(id){
        super();
        this.id = id;
        this.level = 0;
        this.status = 0;
    }

    setValue(level, status = 1){
        this.level = level;
        this.status = status;
        this.emit('change', this.toJSON());
    }

    toJSON(){
        return {
            "device_id": String(this.id),
            "values": this.status ? 
                    [String(this.status), String(this.level)] : 
                    [String(this.status), "0"]
        };
    }
}

module.exports = Speaker;