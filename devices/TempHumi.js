const {EventEmitter} = require('events');
/**
 * TempHumi device
 */
class TempHumi extends EventEmitter{
    constructor(id){
        super();
        this.id = id;
        this.temp = 0;
        this.humi = 0;
    }

    setValue(temp, humi, status = 1){
        this.temp = temp;
        this.humi = humi;
        this.status = status;
        this.emit('change', this.toJSON());
    }

    toJSON(){
        return {
            "device_id": String(this.id),
            "values": [String(this.temp), String(this.humi)]
        };
    }
}

module.exports = TempHumi;